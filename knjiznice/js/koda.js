/*koda je grozna kdorkoli jo gre actually pregledvat se mi smils
ALSO nisem uporabil funkcije za genereranje*/
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var qrUrl = "https://api.qrserver.com/v1/"
var queryUrl = baseUrl + '/query';
var globalNumber = 0;
var usingBar = 1;
var myBarChart; 
var predelanih = 0;
var arrayLength = 100;
var i=0;



var username = "ois.seminar";
var password = "ois4fri";

var bolnik;
var trenutniPacient;
var meritve = ["height","weight","body_temperature","blood_pressure"];

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
    return "Basic " + btoa(username + ":" + password);
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
/*function generirajPodatke(stPacienta) {
    ehrId = "";

    // TODO: Potrebno implementirati

    return ehrId;
}*/


// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija

function updateGraf(podatki){
    //console.log(podatki)
    //console.log("zdej smo v grafu")
    myBarChart.data = podatki;
    myBarChart.update()
}

function predeliPodatkeZaGraf(imePacient,imeLjubezen,pacient,ljubezen){
    
        var pacientPodatki;
        var ljubavPodatki;
        
        //console.log(pacient)
        
        if(imePacient===""){
            imePacient= "Pacient";
        }
        if(imeLjubezen === ""){
            imeLjubezen = "Ljubav";
        }
        if(pacient.length==0){
            pacientPodatki = [0, 0, 0, 0]
        }else{
            pacientPodatki= [pacient[0][0].height, pacient[1][0].weight, pacient[2][0].temperature, pacient[3][0].systolic, pacient[3][0].diastolic]
            //console.log(pacientPodatki)
        }
        if(ljubezen.length == 0){
            ljubavPodatki = [0, 0, 0, 0]
        }else{
            try{
            ljubavPodatki= [ljubezen[0][0].height, ljubezen[1][0].weight, ljubezen[2][0].temperature, ljubezen[3][0].systolic, ljubezen[3][0].diastolic]
            }catch(err){
                $("#alertLjubav").html("<span class='alert alert-warning'>Vsi podatki osebe niso na voljo</span>")
            }
        }
       
        
        var podatki = {
                      labels: ['Telesna višina(cm)', 'Teža(kg)', 'Temperatura(C)', 'Sistolični kt(mm Hg)', 'Diastolični kt(mm Hg)'],
                      datasets:[{
                          label: imePacient,
                          data:pacientPodatki,
                          backgroundColor: "blue"
                      },{
                        label: imeLjubezen,
                        backgroundColor: "red",
                        data:ljubavPodatki
                        
                      }]
                  }
                  console.log(podatki)
                  return podatki
        

    
}

function vrniVsePripadnikeSpola(){
	//tukaj klices nekje not spol iz nekega input okna
	var spol = "FEMALE";
	if($("#izbiraSpola").is(":checked")){
		//ole.log("trueeeee");
		spol = "MALE";
	}
	$("#barLabela").text("Pridobivanje vseh oseb iz EHR");

	globalNumber = 0;
	
	return new Promise(function (resolve,reject){
		
	
		$.ajax({
				url: baseUrl + "/demographics/party/query?gender="+spol,
				type: 'GET',
				headers: {
	        "Authorization": getAuthorization()
	      },
	    	success: function (data) {
	    	    ////ole.log(JSON.stringify(data.parties));
	    	    ////ole.log(Object.keys(data.parties).length);
	    	    globalNumber = 20;
	    	    var session = data;
	    	    //TUKI SE VRNEMO V PREJSNO FUNKCIJO CE JE GUT
	    	    resolve(session);
	    	    
	    	    
	    	    ////ole.log(JSON.stringify(data.parties[10000].firstNames));
	  			
	  		},
	  		error: function(err) {
	  			//SE VRNEMO V PREJSNO FUNKCIJO Z REJECTOM
	  			reject("Error = " + JSON.parse(err.responseText));
	  			/*$("#bigBoi").html("<span class='alert alert-danger'>Napaka '" +
	          JSON.parse(err.responseText).userMessage + "'!");*/
	  		}
		});
	})	
}

//uporablja global nmumber
async function gumbKliknjen(){
	//neka funkcija se poklice
	$("#nadLoadBarom").html("");
	if($("#listMeril").val() && bolnik != undefined){
		console.log("izvaja se")
		poisciMatch($("#listMeril").val())
	
	//poklice se funkcija za risanje loadbara ki gleda na sekundo stanje stevilke global number
		asyRisiBar("iskanjeParaLoadBar");
		
		//updateGraf();
	}else{
		console.log("ne izvaja se")
		if(bolnik == undefined){
		    $("#nadLoadBarom").html("<span class='alert alert-warning'>Prosimo izberite bolnika</span>")
		}else{
		    $("#nadLoadBarom").html("<span class='alert alert-warning'>Prosimo izberite glavno merilo</span>")
		}
	}
}

//ko sem konec z celim procesom nastavim usingBar na 0
async function asyRisiBar(zeleniID){
	//glej vrednost global ki jo bo funkcija za obdelavo pickdelala
	var before = 0;
	while(1){
		if(usingBar){
			if(before != globalNumber){
				$("#" + zeleniID).css("width",String(globalNumber)+"%");
				//ole.log("we in here");
				before = globalNumber;
				console.log("checking in")
			}
		}else{
			return 0;
			
		}
		await sleep(1000);
		
	}
}

function najdiParameter(meritev){
    console.log(bolnik)
    if(meritev =="body_temperature"){
        meritev ="temperature"
    }
    return new Promise(function(resolve){   
        for(var co = 0; co<bolnik.length;co++){
            try{
                
                console.log("yep "+ meritev)
                if(bolnik[co][0][meritev] == undefined){
                    continue;
                }
                console.log(bolnik[co][0][meritev])
                resolve(bolnik[co][0][meritev])
            }catch(err){
                console.log("nope")  
            }
            
        }
    })
}


async function checkIfInRange(bolnikovaMeritev,array,meritev){
	return new Promise( function(resolve,reject){
	    $("#iskanjeParaLoadBar").text("Preverjnaje ujemanja");
	    var potentialLovers = [];
	    //console.log("checking if in range");
	    globalNumber = 60;
	    //console.log("bolnikovaMeritev");
	    //console.log(array[0][0])
	    //console.log(array[0][1]);
	    for(var co = 0; co<array.length;co++){
	        globalNumber +=0.1; 
	        try{
	            console.log(Math.abs(array[co][1]-bolnikovaMeritev))
    	        if(Math.abs(array[co][1]-bolnikovaMeritev)<10){
    	            potentialLovers.push(array[co][0]);
	        }
	        }catch(err){
	            console.log(err);
	        }
	    }
	    if(potentialLovers.length == 0){
	        reject("No suitable lovers press Match button again to go through another 100 :) GL!")
	    }else{
	        globalNumber = 70;
	        resolve(potentialLovers)
	    }
	})
	
}

function izracunajLength(data){
    return new Promise(function(resolve,reject){
        try{
            console.log(data.length)
            resolve(data.length)
        }catch(err){
            reject(err)
        }
    })
}

async function predeliJsonTabelo(data , meritev){
    //za 500 bomo skakal
    var length = await izracunajLength(data.parties);
    console.log(length)
	var loopLength= predelanih +100;
	await sleep(500);
	if(loopLength>length) loopLength = length;
	$("#nadLoadBarom").html("<div class='alert alert-warning'>Iskanje 100 primerkov z vpisano " +meritev +" vseh primerkov je:" + await String(length) + "</div>")
	//var onepercent = loopLength/100;
	var person;
	$("#iskanjeParaLoadBar").text("Pridobivanje informacij vseh oseb");
	var arrayOfPeople = [];
	while(i<arrayLength){
		//if(predelanih == 0) await sleep(500)
		//console.log("neki se dogaja" + String(globalNumber));
		//PRAVILNO
		//console.log(data.parties[predelanih].additionalInfo.ehrId)
		try{
		    var measurment = await returnMeasurmentsForPerson(data.parties[predelanih].additionalInfo.ehrId, meritev)
			if(measurment.length>0){
			    if(meritev =="body_temperature"){
			        person = [data.parties[predelanih].additionalInfo.ehrId , measurment[0]["temperature"]]
    			    i++
    			    arrayOfPeople.push(person);
    			    globalNumber += 0.4;
			    }else{
    			    console.log(measurment)
    			    //console.log(measurment[0][meritev])
    			    //pushamo ehr in pa measurment
    			    person = [data.parties[predelanih].additionalInfo.ehrId , measurment[0][meritev]]
    			    i++
    			    arrayOfPeople.push(person);
    			    globalNumber += 0.4;
			    }
			}
			//arrayOfPeople.push(["ehrId : " + data.parties[predelanih].additionalInfo.ehrId, meritev +" : "+JSON.parse(measurment)]);
			
			//console.log(measurment)
			try{
			    //arrayOfPeople[predelanih].push();
			}catch(error){
			    console.log(error)
			    predelanih++;
			    continue;
			}
			    
		}catch(err){
		    console.log(err)
		    break;
		}
			
		predelanih++;
		
	}
	arrayLength +=100;
	//console.log(arrayOfPeople)
	globalNumber = 60;
	////ole.log(JSON.stringify(arrayOfPeople));
	//ole.log(arrayOfPeople);
	return new Promise(function (resolve, reject){
		resolve(arrayOfPeople)
	})
}

async function vstaviIzborZena(potencial){
    var dodajIme;
    globalNumber = 70;
    $("#iskanjeParaLoadBar").text("Polnjenje seznama Izberi ljubimca");
    $("#ljubimci").html("");
    if(potencial === undefined){
        $("#nadLoadBarom").html("<div class='alert alert-warning'>Ni bilo ujemanj kliknite Match se enkrat za iskanje na novih 100 primerkih ali pa zamenjajte kategorijo :)</div>")
        globalNumber = 0;
        return 0;
        
    }
        for(var co = 0; co<potencial.length;co++){
            globalNumber += 30/potencial.length
            dodajIme = await asyncReturnPersonWithEhrId(potencial[co]);
            
            console.log(dodajIme)
            $("#ljubimci").append('<option value="' + potencial[co] +'">' +dodajIme.firstNames +' ' + dodajIme.lastNames +'</option>')
        }

    $("#iskanjeParaLoadBar").text("DONE!");
}

//zakaj sem to funkcijo naredu asynhrono? bog ve :D
async function poisciMatch(meritev){
	    $("#ljubimci").html("");
            try{
                
        	    var jsonVsehDanegaSpola = await vrniVsePripadnikeSpola();
                var jsonVsehSpolazTezo = await predeliJsonTabelo(jsonVsehDanegaSpola,meritev);
                //console.log(jsonVsehSpolazTezo)
                var bolnikovaMeritev = await najdiParameter(meritev);
                console.log(jsonVsehSpolazTezo)
                try{
                    var jsonUjemajocih = await checkIfInRange(bolnikovaMeritev,jsonVsehSpolazTezo, meritev);
                }catch(err){
                    console.log(err)
                }
                    vstaviIzborZena(jsonUjemajocih);
                //console.log(jsonUjemajocih);
            }catch(err){
            	console.log("problem v poisciMatch " + err.responseText);
            }
	    
	
}

async function izberiBolnika(){
    //pridobimo podatke o bolniku
    predelanih = 0;
    if($("#EHRinput").val()){
        bolnik = [];
        trenutniPacient = await $("#EHRinput").val();
        //console.log(bolnik)
        try{for(var i = 0; i<meritve.length;i++){
            bolnik.push(await returnMeasurmentsForPerson(trenutniPacient,meritve[i]));
            //console.log(bolnik);
            }
        }catch(err){
            console.log("ne obstaja")
            $("#prompt").html("<span class='alert alert-warning'>Vpisani EHRid ne obstaja!</span>")
            console.log(JSON.stringify(err));
        }
        var imePacient = await asyncReturnPersonWithEhrId(trenutniPacient)
        updateGraf(predeliPodatkeZaGraf(imePacient.firstNames,"",bolnik,[]));
    }else{
        $("#prompt").html("<span class='alert alert-warning'>Prosimo vstavite EHRid</span>")
    }
}

async function vstaviLjubav(){
    $("#alertLjubav").html("");
    if($("#ljubimci").val()){
        console.log($("#ljubimci").val())
        var ehrId = $("#ljubimci").val();
        var ljubav = []
        generirajQRLjubav(ehrId)
        try{
            for(var co = 0; co<meritve.length; co++){
                ljubav.push(await returnMeasurmentsForPerson(ehrId,meritve[co]));
                
            }
        }catch(err){
            $("#alertLjubav").html("<span class='alert alert-warning'>Vsi podatki osebe niso na voljo</span>")
        }
        var pacient = await asyncReturnPersonWithEhrId(trenutniPacient);
        var ljubezen = await asyncReturnPersonWithEhrId(ehrId);
        console.log(ljubav);
        var podatki = predeliPodatkeZaGraf(pacient.firstNames,ljubezen.firstNames,bolnik,ljubav);
        updateGraf(podatki);
        
    }else{
        $("#alertLjubav").html("<span class='alert alert-warning'>Prvo je treba poiskati ujemanja</span>")
    }
    
}

function asyncReturnPersonWithEhrId(ehrId) {
	return new Promise(function(resolve, reject){
		////ole.log(ehrId);
		//ole.log("person with ehr klican!")
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {
        	"Authorization": getAuthorization()
    		},
    		success: function (data) {
    			////ole.log("Dodajanje bolnika " + JSON.stringify(data.party));
    			//ole.log("resolve was used")
    			resolve(data.party);
	  		},
	  		error: function(err) {
	  			//ole.log("we got an error 178!")
	  			//ole.log(err)
	  			reject(err);
	  				
	  			}
		});
	});
}

async function generirajQR(){
    
    if($("#EHRinput").val()){
        $("#insertQr").html('<img src="'+ qrUrl+'/create-qr-code/?data=' + encodeURIComponent($("#EHRinput").val())+'&size=100x100">')
        
    }else{
        $("#prompt").html('<span class="alert alert-warning">Prosimo vstavite EHRid</span>')
    }
}

async function generirajQRLjubav(ehrID){
    $("#insertQrLjubav").html('<img src="'+ qrUrl+'/create-qr-code/?data=' + encodeURIComponent(ehrID)+'&size=100x100">')
}

function returnMeasurmentsForPerson(ehrId,meritev){
    return new Promise (function(resolve,reject){
        //console.log(meritev)
        $.ajax({
    				  url: baseUrl + "/view/" + ehrId + "/" +meritev,
    			    type: 'GET',
    			    headers: {
                "Authorization": getAuthorization()
              },
              success:function(res){
                  resolve(res);
              },
              error:function(err){
                  reject(err);
                  console.log("ne obstaja")
              }
              
        });
    });
}

$(document).ready(function(){
    myBarChart = new Chart(document.getElementById("MyCanvas").getContext("2d"), {
                  type: 'bar',
                  data: podatki,
                  options: {}
              });
    var podatki = predeliPodatkeZaGraf("","",[],[]);
    updateGraf(podatki);
    
    $("#preberiObstojeciEhr").change(function(){
        //console.log("somethings changed");
        $("#EHRinput").val($(this).val());
        $("#insertQr").html("")
    })
    
})
